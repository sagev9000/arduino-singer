//
// Created by sage on 8/4/22.
//
#ifndef ARDUINO_SINGER_NOTES_H
#define ARDUINO_SINGER_NOTES_H

#define C3 261
#define D3 293
#define E3 329
#define F3 349
#define FS3 370
#define G3 392
#define A3 440
#define B3 494

#undef F

#define A A3
#define B B3
#define C C3
#define D D3
#define E E3
#define F F3
#define G G3

#define A2 (A3 / 2)
#define B2 (B3 / 2)
#define C2 (C3 / 2)
#define D2 (D3 / 2)
#define E2 (E3 / 2)
#define F2 (F3 / 2)
#define FS2 (FS3 / 2)
#define G2 (G3 / 2)

#define A4 (A3 * 2)
#define B4 (B3 * 2)
#define C4 (C3 * 2)
#define D4 (D3 * 2)
#define E4 (E3 * 2)
#define F4 (F3 * 2)
#define FS4 (FS3 * 2)
#define G4 (G3 * 2)

#define REST 0

#define QUARTER (60000)
#define EIGHTH (QUARTER / 2)
#define SIXTEENTH (EIGHTH / 2)
#define TRIP (QUARTER / 3)
#define HALF (QUARTER * 2)
#define WHOLE (HALF * 2)

struct Note
{
    int pitch;
    long holdFor;
};

struct Song
{
    struct Note* notes;
    int noteCount;
    int tempo;
};

#define BUILD_SONG(TARGET_SONG, TARGET_TEMPO) (struct Song) { \
  .notes = (TARGET_SONG), \
  .noteCount = sizeof(TARGET_SONG) / sizeof((TARGET_SONG)[0]), \
  .tempo = (TARGET_TEMPO) \
}

#endif // ARDUINO_SINGER_NOTES_H