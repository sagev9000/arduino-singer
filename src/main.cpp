#include <Arduino.h>

#include "notes.h"

struct Note n(int pitch, long holdFor)
{
    return (Note) {
        .pitch = pitch,
        .holdFor = holdFor,
    };
}

Note jingleBellsNotes[] = {
  n(E, QUARTER),  n(E, QUARTER),  n(E, HALF),
  n(E, QUARTER),  n(E, QUARTER),  n(E, HALF),
  n(E, QUARTER),  n(G, QUARTER),  n(C, QUARTER),  n(D, QUARTER),
  n(E, WHOLE),

  n(F, QUARTER),  n(F, QUARTER),  n(F, QUARTER),  n(F, QUARTER),
  n(F, QUARTER),  n(E, QUARTER),  n(E, QUARTER),  n(E, EIGHTH),  n(E, EIGHTH),
  n(E, QUARTER),  n(D, QUARTER),  n(D, QUARTER),  n(E, QUARTER),
  n(D, HALF),  n(G, QUARTER), n(REST, QUARTER),

  n(E, QUARTER),  n(E, QUARTER),  n(E, HALF),
  n(E, QUARTER),  n(E, QUARTER),  n(E, HALF),
  n(E, QUARTER),  n(G, QUARTER),  n(C, QUARTER),  n(D, QUARTER),
  n(E, WHOLE),

  n(F, QUARTER),  n(F, QUARTER),  n(F, QUARTER),  n(F, QUARTER),
  n(F, QUARTER),  n(E, QUARTER),  n(E, QUARTER),  n(E, EIGHTH),  n(E, EIGHTH),
  n(G, QUARTER),  n(G, QUARTER),  n(E, QUARTER),  n(D, QUARTER),
  n(C, WHOLE),

  n(REST, WHOLE)
};
struct Song jingleBells = BUILD_SONG(jingleBellsNotes, 200);

// TEMPO
Note youSendMeNotes[] = {
    // VERSE 1:

    // dar         -ling
    n(B, EIGHTH), n(D4, EIGHTH),

    // you
    n(E4, QUARTER), n(D4, EIGHTH), n(E4, EIGHTH), n(D4, HALF),

    // send        me             .                 I             kno        -o
    n(B, QUARTER), n(A, QUARTER), n(REST, QUARTER), n(D4, TRIP), n(B, TRIP), n(D4, TRIP),

    // you
    n(E4, QUARTER), n(D4, EIGHTH), n(E4, EIGHTH), n(D4, HALF),

    // send        me             .                 Dar           -ling
    n(B, QUARTER), n(A, QUARTER), n(REST, QUARTER), n(B, EIGHTH), n(D4, EIGHTH),

    // you
    n(E4, QUARTER), n(D4, EIGHTH), n(E4, EIGHTH), n(D4, HALF),

    // send        me             .                 hon          -est        you
    n(B, QUARTER), n(A, QUARTER), n(REST, QUARTER), n(A, TRIP), n(G, TRIP), n(E, TRIP),

    // do          hon         -est        you         do             hon         -est        you
    n(G, QUARTER), n(G, TRIP), n(A, TRIP), n(G, TRIP), n(E, QUARTER), n(G, TRIP), n(A, TRIP), n(B, TRIP),

    // do
    n(A, QUARTER),

    // whoa
    n(G4, EIGHTH), n(FS4, EIGHTH), n(E4, TRIP), n(G4, TRIP), n(FS4, TRIP), n(E4, QUARTER),

    // VERSE 2:

    // you
    n(E4, QUARTER), n(D4, EIGHTH), n(E4, EIGHTH), n(D4, HALF),

    // thrill      me             .                 I             kno        -o
    n(B, QUARTER), n(A, QUARTER), n(REST, QUARTER), n(D4, TRIP), n(B, TRIP), n(D4, TRIP),

    // you
    n(E4, QUARTER), n(D4, EIGHTH), n(E4, EIGHTH), n(D4, HALF),

    // thrill      me             .                 Dar           -ling
    n(B, QUARTER), n(A, QUARTER), n(REST, QUARTER), n(B, EIGHTH), n(D4, EIGHTH),

    // you
    n(E4, QUARTER), n(D4, EIGHTH), n(E4, EIGHTH), n(D4, HALF),

    // thrill      me             .                 hon         -est        you
    n(B, QUARTER), n(A, QUARTER), n(REST, QUARTER), n(A, TRIP), n(G, TRIP), n(E, TRIP),

    n(G, QUARTER + EIGHTH), n(A, EIGHTH), n(G, HALF),

    n(REST, WHOLE)
};
struct Song youSendMe = BUILD_SONG(youSendMeNotes, 96);

// TEMPO 96
Note soundOfSettlingNotes[] = {
    n(REST, EIGHTH), n(G, EIGHTH), n(G, EIGHTH), n(G, EIGHTH), n(A, EIGHTH), n(B, QUARTER + EIGHTH),

    n(REST, EIGHTH), n(A, EIGHTH), n(A, EIGHTH), n(A, EIGHTH), n(A, EIGHTH), n(G, EIGHTH), n(A, SIXTEENTH), n(B, SIXTEENTH), n(G, EIGHTH + QUARTER),

    n(G, EIGHTH), n(G, EIGHTH), n(G, EIGHTH), n(D, EIGHTH), n(D, EIGHTH), n(E, EIGHTH + QUARTER),
    // Just some insruments???
    n(E, EIGHTH), n(A2, EIGHTH), n(G, EIGHTH), n(C, EIGHTH), n(G, EIGHTH), n(C, EIGHTH),

    n(REST, EIGHTH), n(G, EIGHTH), n(G, EIGHTH), n(G, SIXTEENTH), n(A, SIXTEENTH + EIGHTH), n(B, QUARTER + EIGHTH),

    n(REST, SIXTEENTH), n(G, SIXTEENTH), n(A, EIGHTH), n(A, EIGHTH), n(A, EIGHTH), n(A, EIGHTH), n(G, EIGHTH), n(A, SIXTEENTH), n(B, SIXTEENTH), n(G, EIGHTH),

    n(REST, QUARTER), n(G, EIGHTH), n(G, EIGHTH), n(G, EIGHTH), n(D, EIGHTH), n(D, EIGHTH), n(B2, EIGHTH),

    n(E, EIGHTH), n(G, QUARTER), n(A, QUARTER), n(G, QUARTER + EIGHTH),

    n(REST, QUARTER), n(G, EIGHTH), n(D4, EIGHTH + HALF),
    //               this           is             the           sound          of             set              -tl              -ing
    n(REST, EIGHTH), n(D4, EIGHTH), n(D4, EIGHTH), n(B, EIGHTH), n(E4, EIGHTH), n(D4, EIGHTH), n(B, SIXTEENTH), n(A, SIXTEENTH), n(G, EIGHTH),
    // 11
    n(REST, QUARTER), n(G, EIGHTH), n(D4, EIGHTH + QUARTER), n(G, EIGHTH), n(D4, EIGHTH + WHOLE),

    n(REST, QUARTER), n(G, EIGHTH), n(D4, EIGHTH + QUARTER), n(G, EIGHTH), n(D4, EIGHTH + EIGHTH),

    n(D4, EIGHTH), n(D4, EIGHTH), n(B, EIGHTH), n(E4, EIGHTH), n(D4, EIGHTH), n(B, SIXTEENTH), n(A, SIXTEENTH), n(G, EIGHTH),

    n(REST, QUARTER), n(G, EIGHTH), n(D4, EIGHTH + QUARTER), n(G, EIGHTH), n(D4, EIGHTH + QUARTER),

    // n(G, EIGHTH), n(D4, EIGHTH + QUARTER),

    n(REST, WHOLE)
};
struct Song soundOfSettling = BUILD_SONG(soundOfSettlingNotes, 96);

struct Song* songs[] = {&jingleBells, &youSendMe, &soundOfSettling};
int songCount = sizeof(songs) / sizeof(songs[0]);

#define A_FUNC __attribute__((unused))

#define GREEN_PIN 10
#define SPEAKER_PIN 11
#define RED_PIN 12
#define BUTTON_PIN 8

// the setup function runs once when you press reset or power the board
A_FUNC void setup()
{
    pinMode(SPEAKER_PIN, OUTPUT);
    pinMode(GREEN_PIN, OUTPUT);
    pinMode(RED_PIN, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(BUTTON_PIN, INPUT_PULLUP);
}

// Add a slight pause between notes so they don't blend into each other
#define BETWEEN_PAUSE 5

void playSong(struct Song* song)
{
    int redLed = 1;
    for (int i = 0; i < song->noteCount; i++) {
        // Rest
        if (!song->notes[i].pitch) {
            digitalWrite(GREEN_PIN, LOW);
            digitalWrite(RED_PIN, LOW);
            noTone(SPEAKER_PIN);
            delay((song->notes[i].holdFor / song->tempo) + BETWEEN_PAUSE);
            continue;
        }

        tone(SPEAKER_PIN, song->notes[i].pitch);

        digitalWrite(RED_PIN, redLed ? HIGH : LOW);
        digitalWrite(GREEN_PIN, redLed ? LOW : HIGH);
        redLed = !redLed;

        delay(song->notes[i].holdFor / song->tempo);

        noTone(SPEAKER_PIN);
        delay(BETWEEN_PAUSE);
    }
}

int playing = 0;
int songIndex = 0;
A_FUNC void loop()
{
    playing = !digitalRead(BUTTON_PIN);
    if (!playing) {
        delay(100);
        return;
    }
    playSong(songs[songIndex]);
    songIndex = (songIndex + 1) % songCount;
    playing = 0;
}
